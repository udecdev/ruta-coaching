window.onload = function() {
    // Código a ejecutar cuando se ha cargado toda la página
  document.body.style.visibility = "visible";
   // stage2.stop();
    main();
};
var audios = [{
    url: "./sounds/click.mp3",
    name: "clic"
}];
ivo.info({
    title: "Universidad de Cindinamarca",
    autor: "Edilson Laverde Molina",
    date: "",
    email: "edilsonlaverde_182@hotmail.com",
    icon: 'https://www.ucundinamarca.edu.co/images/iconos/favicon_verde_1.png',
    facebook: "https://www.facebook.com/edilson.laverde.molina"
});
var stage1 = new TimelineMax();
var stage2 = new TimelineMax();
var stage3 = new TimelineMax();
var stage4 = new TimelineMax();
var op1 = new TimelineMax();
var op2 = new TimelineMax();
var op3 = new TimelineMax();
var op4 = new TimelineMax();
var op5 = new TimelineMax();
function main() {
    var t = null;
    var udec = ivo.structure({
        created: function () {

            t = this;
            
            t.animations();
            t.events();
            //precarga audios//
            var onComplete = function () {
                ivo("#preload").hide();
                stage1.play();
            };
            ivo.load_audio(audios,onComplete );
        },
        methods: {
            events: function () {
                ivo("#btn_home").on("click", function () {
                    stage2.timeScale(3).reverse();
                    stage1.timeScale(1).play();
                    ivo.play("clic");
                });
                ivo("#stage1__btn").on("click", function () {
                    stage1.timeScale(3).reverse();
                    stage2.timeScale(1).play();
                    ivo.play("clic");
                });
                ivo("#btn_credits").on("click", function () {
                    stage3.timeScale(1).play();
                    ivo.play("clic");
                });
                ivo("#credits_close").on("click", function () {
                    stage3.timeScale(3).reverse();
                    ivo.play("clic");
                });
                let rules = function(rule){
                    console.log("rules"+rule);
                    
                };
                let onMove=function(page){
                    console.log("onMove"+page);
                    
                    ivo.play("clic");
                };
                let onFinish=function(){
                }
            
                var slider=ivo("#slider").slider({
                    slides:'.slider',
                    btn_next:"#btn_next",
                    btn_back:"#btn_back",
                    rules:rules,
                    onMove:onMove,
                    onFinish:onFinish
                });

                ivo("#p-5 .close").on("click", function () {
                    op5.timeScale(3).reverse();
                    ivo.play("clic");
                });
                ivo("#p-4 .close").on("click", function () {
                    op4.timeScale(3).reverse();
                    ivo.play("clic");
                });
                ivo("#p-3 .close").on("click", function () {
                    op3.timeScale(3).reverse();
                    ivo.play("clic");
                });
                ivo("#p-2 .close").on("click", function () {
                    op2.timeScale(3).reverse();
                    ivo.play("clic");
                });
                ivo("#p-1 .close").on("click", function () {
                    op1.timeScale(3).reverse();
                    ivo.play("clic");
                });

                ivo("#cir5").on("click", function () {
                    op5.timeScale(1).play();
                    ivo.play("clic");
                    ivo(".topic").css("background-image","url(./images/b-5.png)"  );
                }).on("mousemove",function(){
                    ivo(this).css("scale","1.2");
                }).on("mouseout",function(){
                    ivo(this).css("scale","1");
                }).css("cursor","pointer");

                ivo("#cir4").on("click", function () {
                    op4.timeScale(1).play();
                    ivo.play("clic");
                    ivo(".topic").css("background-image","url(./images/b-4.png)"  );
                }).on("mousemove",function(){
                    ivo(this).css("scale","1.2");
                }).on("mouseout",function(){
                    ivo(this).css("scale","1");
                }).css("cursor","pointer");

                ivo("#cir3").on("click", function () {
                    op3.timeScale(1).play();
                    ivo.play("clic");
                    ivo(".topic").css("background-image","url(./images/b-3.png)"  );
                }).on("mousemove",function(){
                    ivo(this).css("scale","1.2");
                }).on("mouseout",function(){
                    ivo(this).css("scale","1");
                }).css("cursor","pointer");

                ivo("#cir2").on("click", function () {
                    op2.timeScale(1).play();
                    ivo.play("clic");
                    ivo(".topic").css("background-image","url(./images/b-2.png)"  );
                }).on("mousemove",function(){
                    ivo(this).css("scale","1.2");
                }).on("mouseout",function(){
                    ivo(this).css("scale","1");
                }).css("cursor","pointer");

                ivo("#cir1").on("click", function () {
                    op1.timeScale(1).play();
                    ivo.play("clic");
                    ivo(".topic").css("background-image","url(./images/b-1.png)"  );
                   
                }).on("mousemove",function(){
                    ivo(this).css("scale","1.2");
                }).on("mouseout",function(){
                    ivo(this).css("scale","1");
                }).css("cursor","pointer");
                
                ivo("#cir6").on("mousemove",function(){
                    ivo(this).css("scale","1.2");
                }).on("mouseout",function(){
                    ivo(this).css("scale","1");
                });

                ivo(".info").on("click", function () {
                    //traemos el data-modal
                    let modal = ivo(this).attr("data-modal");
                    ivo("#image1").hide();
                    ivo("#"+modal).show();
                    stage4.timeScale(1).play();
                    ivo.play("clic");
                  });
                  ivo(".close").on("click", function () {
                    ivo.play("clic");
                    stage4.timeScale(6).reverse();
                  });
            },
            animations: function () {

                stage1.append(TweenMax.from("#stage1", .8, {y: 1300, opacity: 0}), 0);
                stage1.append(TweenMax.staggerFrom("#stage1__title", .8, {y: 1300, opacity: 0}, .2), 0);
                stage1.append(TweenMax.from("#stage1__btn", .8, {x: 1300, opacity: 0,rotation:900}), 0);
                stage1.stop();
              
                stage2.append(TweenMax.from("#stage2", .8, {y: 7300, opacity: 0}), 0);
                stage2.stop();
              
                stage3.append(TweenMax.from("#modal-credits", .8, {y: 7300, opacity: 0}), 0);
                stage3.append(TweenMax.from("#credits_close", .8, {x: 300, opacity: 0}), 0);
                stage3.stop();

                op1.append(TweenMax.from("#p-1", .8, {y: 1300, opacity: 0}), 0);
                op1.append(TweenMax.from("#p-1 div", .8, {x: 1300, opacity: 0}), 0);
                op1.append(TweenMax.from("#p-1 .close", .8, {x: 300, opacity: 0,rotation:900}), 0);
                op1.stop();
                
                op2.append(TweenMax.from("#p-2", .8, {y: 1300, opacity: 0}), 0);
                op2.append(TweenMax.from("#p-2 div", .8, {x: 1300, opacity: 0}), 0);
                op2.append(TweenMax.from("#p-2 .close", .8, {x: 300, opacity: 0,rotation:900}), 0);
                op2.stop();

                op3.append(TweenMax.from("#p-3", .8, {y: 1300, opacity: 0}), 0);
                op3.append(TweenMax.from("#p-3 div", .8, {x: 1300, opacity: 0}), 0);
                op3.append(TweenMax.from("#p-3 .close", .8, {x: 300, opacity: 0,rotation:900}), 0);
                op3.stop();
                
                op4.append(TweenMax.from("#p-4", .8, {y: 1300, opacity: 0}), 0);
                op4.append(TweenMax.from("#p-4 div", .8, {x: 1300, opacity: 0}), 0);
                op4.append(TweenMax.from("#p-4 .close", .8, {x: 300, opacity: 0,rotation:900}), 0);
                op4.stop();
                
                op5.append(TweenMax.from("#p-5", .8, {y: 1300, opacity: 0}), 0);
                op5.append(TweenMax.from("#p-5 div", .8, {x: 1300, opacity: 0}), 0);
                op5.append(TweenMax.from("#p-5 .close", .8, {x: 300, opacity: 0,rotation:900}), 0);
                op5.stop();

                stage4.append(TweenMax.from(".modal", .8, {y: 7300, opacity: 0}), 0);
                stage4.append(TweenMax.from(".close", .8, {x: 300, opacity: 0}), 0);
                stage4.stop();

                let movil = false;
                let windowWidth = window.innerWidth;
                if (windowWidth < 1024) {
                    movil = true;
                    
                }
                if('ontouchstart' in window || navigator.maxTouchPoints) {
                    movil = true;
                }
                if (movil) {
                    stage1.play();
                    stage2.play();
                    stage3.play();
                    op1.play();
                    op2.play();
                    op3.play();
                    op4.play();
                    op5.play();
                }

            }
        }
    });
}